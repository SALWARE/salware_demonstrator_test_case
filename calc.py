from Tkinter import *
import tkFont
from ttk import *
import board_commands


def click(key):
    if key == '=':
        # avoid division by integer
        if '/' in entry.get() and '.' not in entry.get():
            entry.set(entry.get()+".0")
        # guard against the bad guys abusing eval()
        str1 = "-+0123456789."
        if entry.get()[0] not in str1:
            entry.set(entry.get()+"first char not in " + str1)
        # here comes the calculation part
        (operand_1, operand_2) = [int(i) for i in entry.get().split("*")]
        tclsh.eval(board_manager.source_tcl_package())
        with open("com_port.txt") as com_file:
            for line in com_file:
                com_port=line.strip()
        tclsh.eval(board_manager.connect(com_port))
        tclsh.eval("set data \"[fromHEX {"+hex(operand_1)[2:].zfill(16).upper()+"}]\n\
        [fromHEX {"+hex(operand_2)[2:].zfill(16).upper()+"}]\"")

        tclsh.eval("set puf_mode     3\n")
        tclsh.eval("set mb2db_words  2\n")
        tclsh.eval("set db2mb_words  2\n")
        tclsh.eval("set ret [sendPufData $dev $puf_mode $mb2db_words $db2mb_words $data 0]\n")
        ret = tclsh.eval("return $ret\n").split(" ")
        tclsh.eval(board_manager.disconnect())
        try:
            if ret[0] != "B2092151":
                print("Error")
            if ret[1] != 32*"0":
                ret[1] = int(ret[1].lstrip("0"), 16)
            else:
                ret[1] = 0
            result = ret[1]
            
            # result = eval(entry.get())
            entry.set(str(result)[-12:])
        except:
            entry.set("Error!")
    elif key == 'C':
        entry.set("")  # clear entry
    else:
        # previous calculation has been done, clear entry
        if '=' in entry.get():
            entry.set("")
        entry.set(entry.get()+key)
    
tclsh = Tcl()
board_manager = board_commands.Board_manager()

root = Tk()
root.title("Simple Multiplier")
btn_list = [
'7',  '8',  '9',
'4',  '5',  '6',
'1',  '2',  '3',
'0',  '*',  'C',
'=']
# create all buttons with a loop
r = 1
c = 0
helv36 = tkFont.Font(family='Helvetica', size=36, weight=tkFont.BOLD)
for b in btn_list:
    rel = 'ridge'
    cmd = lambda x=b: click(x)
    if b == '=':
        Button(root,text=b,width=4,command=cmd).grid(row=r,column=c,columnspan=3,sticky='EWNS')
    else:
        Button(root,text=b,width=4,command=cmd).grid(row=r,column=c,sticky='EWNS')
    c += 1
    if c > 2:
        c = 0
        r += 1
# use Entry widget for an editable display
# entry = Entry(root, font = "Helvetica 36")
entry = StringVar()
display = Label(root, textvariable=entry, font = "Helvetica 36", background = "white")
s = Style()
s.configure('TButton', foreground='black', font = "Helvetica 36")
s.configure('TEntry', foreground='black', font = "Helvetica 36")
display.grid(row=0, column=0, columnspan=3, sticky = "we")
root.mainloop()

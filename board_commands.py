# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: board_commands.py
# Date: 2016-10-27

import Tkinter
import itertools as it
import math

class Board_manager():
    
    """Handle communication with the board

    Board-specific commands must be modified in this file
    
    """

    def source_tcl_package(self):
        command = "source HECTOR_data_acq.tcl\n"
        return command
    
    def connect(self, COM_port):
        command = "set dev [openDevice "+COM_port+"]\n"
        return command
    
    def reset_boards(self, port):
        command = "softReset $dev\n\
selectDaughterBoard $dev "+str(port)+"\n\
sendDaughterReset $dev"
        return command

    def generate_response(self):
        command = "set puf_mode 1\n\
set mb2db_words 0\n\
set db2mb_words 2\n\
set ret [sendPufData $dev $puf_mode $mb2db_words $db2mb_words 0 0]\n\
return $ret"
        return command

    def command_get_parities_from_indices(self, indices, response_length, verbose=False):
        CASCADE_code = {1:"1",
                    2:"2",
                    4:"3",
                    8:"4",
                    16:"5",
                    32:"6",
                    64:"7"}

        nb_blocks = len(indices)
        len_each_block = [len(i) for i in indices]
        block_size = len_each_block[0]

        if nb_blocks*[block_size] != len_each_block:
            raise ValueError("Blocks have different length", block_size, " VS ", len_each_block)
        if block_size not in [1, 2, 4, 8, 16, 32, 64]:
            raise ValueError("Illegal block size")
        data_to_send = bin(nb_blocks)[2:].zfill(8)
        if verbose:print "NBB", data_to_send
        indexes = [item for sublist in indices for item in sublist]
        for index in indexes:
            data_to_send = bin(index)[2:].zfill(7)+data_to_send
        if verbose:print "STR", data_to_send
        data_to_send = int(data_to_send, 2)
        if verbose:print "INT", data_to_send
        data_to_send = hex(data_to_send)[2:]
        if verbose:print "HEX", data_to_send
        if data_to_send[-1] == "L":
            data_to_send = data_to_send[:-1]
            if verbose:print "HEX", data_to_send
        frame_length = int(math.ceil(float(len(data_to_send))/16))
        data_to_send = data_to_send.zfill(frame_length*16)
        command = "set data \""
        while data_to_send:
            command+="[fromHEX {"+data_to_send[:16].upper()+"}]\n"
            data_to_send = data_to_send[16:]
        command+="\"\n"
        command+="set puf_mode \"[fromHEX {"+CASCADE_code[block_size]+"2}]\"\n"
        command+="set mb2db_words  "+str(frame_length)+"\n"
        command+="set db2mb_words  2\n"
        command+="set par [sendPufData $dev $puf_mode $mb2db_words $db2mb_words $data 0]\n"
        # command+="return $par\n"
        command+="return [string reverse [string range [string reverse $par] 0 "+str(int(math.ceil((nb_blocks-1)/4)))+"]]"
        return command

    def activate_design(self, encrypted_AW):
        command = "set puf_mode 4\n"
        command+="set mb2db_words  2\n"
        command+="set db2mb_words  0\n"

        command+="set data \"[fromHEX {"+encrypted_AW[:len(encrypted_AW)/2]+"}]\n"
        command+="[fromHEX {"+encrypted_AW[len(encrypted_AW)/2:]+"}]\"\n"

        command+="set ret [sendPufData $dev $puf_mode $mb2db_words $db2mb_words $data 0]\n"
        command+="set status [lindex $ret 0]\n"
        command+="return $status"

        return command

    def disconnect(self):
        command = "disconnect $dev\n"
        return command
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
